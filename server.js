// import express
const express = require('express');

// Instantiate express
const app = express();
const PORT_NUMBER = process.env.PORT || 3002;
const staticfolder = __dirname + '/public';
console.log(__dirname);
app.use(express.static(staticfolder));


app.get('/cat2',(req,res,next)=>{
    res.sendFile(staticfolder + '/meow.html');
});

app.get('/cat3',(req,res,next)=>{
    res.sendFile(staticfolder + '/meow2.html');
});

app.use((req, res, next)=>{
    res.redirect('/error.html');
});

app.listen(PORT_NUMBER, ()=>{
    console.log(`App listening on ${PORT_NUMBER}`);
})